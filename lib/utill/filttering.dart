import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';

List<Product> filterAndSortProducts(
    List<Product> products,
    int filterIndex,
    double startingPrice,
    double endingPrice,
    ) {
  List<Product> filteredProductList = [];

  print("in the new technique");
  // Apply filtering based on price
  if (startingPrice >= 0 && endingPrice > startingPrice) {
    filteredProductList.addAll(products.where((product) =>
    product.unitPrice! > startingPrice &&
        product.unitPrice! < endingPrice).toList());
  } else {
    filteredProductList.addAll(products);
  }


  // Apply sorting based on filterIndex
  if (filterIndex == 1) {
    filteredProductList.sort((a, b) => a.name!.toLowerCase().compareTo(b.name!.toLowerCase()));
  } else if (filterIndex == 2) {
    filteredProductList.sort((a, b) => a.name!.toLowerCase().compareTo(b.name!.toLowerCase()));
    filteredProductList = filteredProductList.reversed.toList();
  } else if (filterIndex == 3) {
    filteredProductList.sort((a, b) => a.unitPrice!.compareTo(b.unitPrice!));
  } else if (filterIndex == 4) {
    filteredProductList.sort((a, b) => a.unitPrice!.compareTo(b.unitPrice!));
    filteredProductList = filteredProductList.reversed.toList();
  }

  return filteredProductList;
}
