import 'package:flutter/material.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/category/all_category_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/category_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/top_seller_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/all_top_seller_screen.dart';
import 'package:provider/provider.dart';


class ShopsCategoriesScreen extends StatefulWidget {
  const ShopsCategoriesScreen({Key? key}) : super(key: key);

  @override
  State<ShopsCategoriesScreen> createState() => _ShopsCategoriesScreenState();
}

class _ShopsCategoriesScreenState extends State<ShopsCategoriesScreen> {
  final ScrollController _scrollController = ScrollController();

  void passData(int index, String title) {
    index = index;
    title = title;
  }

  bool singleVendor = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Provider.of<ThemeProvider>(context).darkTheme ?
        Colors.black : Theme.of(context).primaryColor,
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(5), bottomLeft: Radius.circular(5))),
        leading: IconButton(icon: const Icon(Icons.arrow_back_ios, size: 20,
            color: ColorResources.white),
          onPressed: () => Navigator.of(context).maybePop(),
        ),
        title: Text(getTranslated('shops', context)!,
            style: titilliumRegular.copyWith(fontSize: 20, color: ColorResources.white)),
      ),
      backgroundColor: ColorResources.getHomeBg(context),
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              controller: _scrollController,
              slivers: [
                // const Padding(padding: EdgeInsets.all(20)),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(Dimensions.homePagePadding,
                        Dimensions.paddingSizeSmall, Dimensions.paddingSizeDefault, Dimensions.paddingSizeSmall  ),
                    child: Column(
                      children: [
                        const SizedBox(height: Dimensions.homePagePadding),

                        // Category
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: Dimensions.paddingSizeExtraExtraSmall,vertical: Dimensions.paddingSizeExtraSmall),
                          child: TitleRow(title: getTranslated('CATEGORY', context),
                              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => const AllCategoryScreen()))),
                        ),
                        const SizedBox(height: Dimensions.paddingSizeSmall),
                        const Padding(
                          padding: EdgeInsets.only(bottom: Dimensions.homePagePadding),
                          child: CategoryView(isHomePage: true),
                        ),

                        const SizedBox(height: Dimensions.paddingSizeExtraLarge),

                        //top seller
                        singleVendor?const SizedBox():
                        TitleRow(title: getTranslated('top_seller', context),
                          onTap: () {Navigator.push(context, MaterialPageRoute(builder: (_) => const AllTopSellerScreen(topSeller: null,)));},),
                        singleVendor?const SizedBox(height: 0):const SizedBox(height: Dimensions.paddingSizeSmall),
                        singleVendor?const SizedBox():
                        const Padding(
                          padding: EdgeInsets.only(bottom: Dimensions.homePagePadding),
                          child: TopSellerView(isHomePage: true),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
