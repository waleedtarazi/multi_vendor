import 'package:flutter/material.dart';

import '../../../../utill/custom_themes.dart';
import '../../../../utill/dimensions.dart';

class TitleButton extends StatelessWidget {
  final String image;
  final String? title;
  final Widget navigateTo;
  const TitleButton({Key? key, required this.image, required this.title, required this.navigateTo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(image, width: 25, height: 25, fit: BoxFit.fill),
      title: Text(title!, style: titilliumRegular.copyWith(fontSize: Dimensions.fontSizeLarge)),
      onTap: () => Navigator.push(
        context, MaterialPageRoute(builder: (_) => navigateTo),
      ),
    );
  }
}
