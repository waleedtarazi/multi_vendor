import 'package:flutter/material.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/custom_expanded_app_bar.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/widget/html_view_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/widget/title_button.dart' as button;
import 'package:provider/provider.dart';

class PoliciesScreen extends StatelessWidget {
  const PoliciesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return CustomExpandedAppBar(title: getTranslated('policies', context),
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [

            Padding(padding: const EdgeInsets.only(top: Dimensions.paddingSizeLarge,
                left: Dimensions.paddingSizeLarge),
              child: Text(getTranslated('policies', context)!,
                  style: titilliumSemiBold.copyWith(fontSize: Dimensions.fontSizeLarge)),
            ),

            Expanded(child: ListView(
              physics: const BouncingScrollPhysics(),
              padding: const EdgeInsets.symmetric(horizontal: Dimensions.paddingSizeExtraSmall),
              children: [
                button.TitleButton(image: Images.privacyPolicy, title: getTranslated('privacy_policy', context),
                    navigateTo: HtmlViewScreen(title: getTranslated('privacy_policy', context),
                      url: Provider.of<SplashProvider>(context, listen: false).configModel!.privacyPolicy,)),

                if(Provider.of<SplashProvider>(context, listen: false).configModel!.refundPolicy!.status ==1)
                  button.TitleButton(image: Images.refundPolicy, title: getTranslated('refund_policy', context),
                      navigateTo: HtmlViewScreen(title: getTranslated('refund_policy', context),
                        url: Provider.of<SplashProvider>(context, listen: false).configModel!.refundPolicy!.content,)),

                if(Provider.of<SplashProvider>(context, listen: false).configModel!.returnPolicy!.status ==1)
                  button.TitleButton(image: Images.returnPolicy, title: getTranslated('return_policy', context),
                      navigateTo: HtmlViewScreen(title: getTranslated('return_policy', context),
                        url: Provider.of<SplashProvider>(context, listen: false).configModel!.returnPolicy!.content,)),

                if(Provider.of<SplashProvider>(context, listen: false).configModel!.cancellationPolicy!.status ==1)
                  button.TitleButton(image: Images.cPolicy, title: getTranslated('cancellation_policy', context),
                      navigateTo: HtmlViewScreen(title: getTranslated('cancellation_policy', context),
                        url: Provider.of<SplashProvider>(context, listen: false).configModel!.cancellationPolicy!.content,)),

              ],
            )),

          ])
    );
  }

}
