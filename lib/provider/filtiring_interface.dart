import 'package:flutter/material.dart';

abstract class Filterable<T> extends ChangeNotifier {
  int get filterIndex;
  void setFilterIndex(int index);
  List<T> filter(List<T> items);
  void sortProductsList(double minPrice, double maxPrice);
}
