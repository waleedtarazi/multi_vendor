import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/base/api_response.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/repository/search_repo.dart';
import 'package:flutter_sixvalley_ecommerce/helper/api_checker.dart';

import '../utill/filttering.dart';
import 'filtiring_interface.dart';

class SearchProvider with ChangeNotifier implements Filterable {
  final SearchRepo? searchRepo;
  SearchProvider({required this.searchRepo});

  int _filterIndex = 0;
  List<String> _historyList = [];

  @override
  int get filterIndex => _filterIndex;
  List<String> get historyList => _historyList;

  @override
  void setFilterIndex(int index) {
    _filterIndex = index;
    notifyListeners();
  }


  @override
  void sortProductsList(double startingPrice, double endingPrice) {
    _searchProductList = [];
    _searchProductList = filterAndSortProducts(_filterProductList!, _filterIndex, startingPrice, endingPrice );
    print("in the sortProductList function in the Search provider class");
    notifyListeners();
  }

  List<Product>? _searchProductList;
  List<Product>? _filterProductList;
  bool _isClear = true;
  String _searchText = '';

  List<Product>? get searchProductList => _searchProductList;
  List<Product>? get filterProductList => _filterProductList;
  bool get isClear => _isClear;
  String get searchText => _searchText;

  void setSearchText(String text) {
    _searchText = text;
    notifyListeners();
  }

  void cleanSearchProduct() {
    _searchProductList = [];
    _isClear = true;
    _searchText = '';
    // notifyListeners();
  }

  void searchProduct(String query, BuildContext context) async {
    _searchText = query;
    _isClear = false;
    _searchProductList = null;
    _filterProductList = null;
    notifyListeners();

    ApiResponse apiResponse = await searchRepo!.getSearchProductList(query);
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      if (query.isEmpty) {
        _searchProductList = [];
        _filterProductList = [];
      } else {
        _searchProductList = [];
        if(ProductModel.fromJson(apiResponse.response!.data).products != null){
          _searchProductList!.addAll(ProductModel.fromJson(apiResponse.response!.data).products!);
          _filterProductList = [];
          _filterProductList!.addAll(ProductModel.fromJson(apiResponse.response!.data).products!);
        }

      }
    } else {
      ApiChecker.checkApi( apiResponse);
    }
    notifyListeners();
  }

  void initHistoryList() {
    _historyList = [];
    _historyList.addAll(searchRepo!.getSearchAddress());

  }

  void saveSearchAddress(String searchAddress) async {
    searchRepo!.saveSearchAddress(searchAddress);
    if (!_historyList.contains(searchAddress)) {
      _historyList.add(searchAddress);
    }
    notifyListeners();
  }

  void clearSearchAddress() async {
    if (kDebugMode) {
      print('search tap');
    }
    searchRepo!.clearSearchAddress();
    _historyList = [];
    notifyListeners();
  }

  @override
  List filter(List items) {
    // TODO: implement filter
    throw UnimplementedError();
  }
}
